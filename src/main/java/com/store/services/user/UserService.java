package com.store.services.user;

import java.util.Date;

import com.store.common.Property;
import com.store.common.Utils;
import com.store.models.user.UserType;

public class UserService {
	
	public static boolean isEmplpoyee(UserType type){
		return type == UserType.EMPLOYEE;
	}
	
	public static boolean isAffiliate(UserType type){
		return type == UserType.AFFIlIATE;
	}
	
	public static boolean isCustomer(UserType type){
		return type == UserType.CUSTOMER;
	}
	
	public static boolean isCustomerMoreThan2Years(UserType type,  Date created){
		return isCustomer(type) && Utils.isCreatedMoreThan2Years(created);
	}
	
	public static double getUserPercentageDiscount(UserType type, Date created){
		if(isEmplpoyee(type)){
			return Double.parseDouble(Property.getEmployeePercentage());
		}
		
		if(isAffiliate(type)){
			return Double.parseDouble(Property.getAffiliatePercentage());
		}
		
		if(isCustomerMoreThan2Years(type, created)){
			return Double.parseDouble(Property.getCustomerPercentage());
		}
		
		return 0;
	}
}
