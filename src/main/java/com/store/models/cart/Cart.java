package com.store.models.cart;

import java.util.ArrayList;

import com.store.models.item.Item;
import com.store.models.user.User;

public class Cart {
	private User user;
	private ArrayList<Item> items = new ArrayList<Item>();
	private Double total;
	private double discountPercentage;
	private Double discountValue;
	private Double grossTotal;
	
	public Cart(User user, ArrayList<Item> items){
		this.user = user;
		this.items = items;
		this.discountPercentage = user.getDiscountPercentage();
	}

	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public ArrayList<Item> getItems() {
		return items;
	}
	public void setItems(ArrayList<Item> items) {
		this.items = items;
	}
	public Double getTotal() {
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	public double getDiscountPercentage() {
		return discountPercentage;
	}
	public void setDiscountPercentage(double discountPercentage) {
		this.discountPercentage = discountPercentage;
	}
	public Double getDiscountValue() {
		return discountValue;
	}
	public void setDiscountValue(Double discountValue) {
		this.discountValue = discountValue;
	}
	public Double getGrossTotal() {
		return grossTotal;
	}
	public void setGrossTotal(Double grossTotal) {
		this.grossTotal = grossTotal;
	}
}
